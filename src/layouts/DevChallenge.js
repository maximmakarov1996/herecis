import React from "react"

import {
  Button,
  Container,
  Row,
  Col,
  Card
} from "reactstrap"

import CardsFooter from "../components/CardsFooter"
import NavigationBar from '../components/NavigationBar'
import InternshipTimeLine from '../components/internship/InternshipTimeLine'
import Schema from '../components/internship/Schema'
import SliderReasons from '../components/internship/SliderReasons'
import Prices from '../components/internship/Prices'


class DevChallenge extends React.Component {
  state = {}
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavigationBar />
        <main ref="main">
          <div className="position-relative">
            {/* shape Hero */}
            <section className="section section-lg section-shaped pb-100">
              <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require("assets/img/challenge/notebook-1280538.jpg")})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                    <Col lg="8">
                      <h1 className="display-3 text-white" >
                        HERE Dev Challenge 2020{" "}
                      </h1>
                      <p className="lead text-white">
                        IT соревнование для студентов, которые хотят развиваться.<br/> <br/>
                        Приглашаем всех желающих, которые хотят пройти практику в международной компании и получить реальный опыт в области аналитики данных <br/> <br/>
                        Регистрация проходит до 24 мая<br/>Конкурс с 25 по 28 мая
                      </p>
                      <div className="btn-wrapper">
                        <Button
                          className="mb-3 mb-sm-0"
                          color="here-pa"
                          href="#"
                          target="_blank"
                 
                        >
                          {/* <span className="btn-inner--icon mr-1">
                            <i className="fa fa-code" />
                          </span> */}
                          <span className="btn-inner--text">Принять участие</span>
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
              {/* 1st Hero Variation */}
            </div>
            <section className="section section-lg">
                <Container fluid={true}>
                    <Row className="text-center justify-content-center" style={{paddingBottom: "60px"}}>
                        <Col lg="10">
                            <h2 className="display-2 ">Возможности</h2>
                        
                        </Col>
                    </Row>
                    <Row className="text-center justify-content-center">
                        <Col lg="10">
                         <SliderReasons />
                        </Col>
                    </Row>
                </Container>
            </section>
           <Schema />
           <Prices />
           <InternshipTimeLine />
           <section className="section section-lg pt-100">
            <Container>
              <Card className="bg-gradient-here-yg shadow-lg border-0">
                <div className="p-5">
                  <Row className="align-items-center">
                    <Col lg="8">
                      <h3 className="text-white">
                        Присоединяйся к конкурсу <br/> HERE Community Challenge 2020!
                      </h3>
                      <p className="lead text-white mt-3">
                          Заполните форму чтобы стать участником конкурса
                      </p>
                    </Col>
                    <Col className="ml-lg-auto" lg="3">
                      <Button
                        block
                        className="btn-white"
                        color="default"
                        target="_blank"
                        href="https://forms.office.com/Pages/ResponsePage.aspx?id=zTRAbSVyck-4U5H-rqZJGegRNy8qPdhNvb6IK_zIuyRUOVhLSkNUVFpJODRUWVA3SDhISjFFRkkxVy4u"
                        size="lg"
                      >
                        Принять участие
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Container>
          </section>

          </main>
        <CardsFooter />
      </>
    )
  }
}

export default DevChallenge
