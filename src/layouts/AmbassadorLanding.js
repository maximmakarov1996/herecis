import React from "react"

import {
  Button,
  Container,
  Row,
  Col,
  Card
} from "reactstrap"

import CardsFooter from "../components/CardsFooter"
import NavigationBar from '../components/NavigationBar'
import WhoAmbassador from "../components/ambassador/WhoAmbassador"
import Reasons from "../components/ambassador/Reasons"
import Tasks from "../components/ambassador/Tasks"
import Ideas from "../components/ambassador/Ideas"


class AmbassadorLanding extends React.Component {
  state = {}
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavigationBar />
        <main ref="main">
          <div className="position-relative">
            {/* shape Hero */}
            <section className="section section-lg section-shaped pb-250">
              <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require('assets/img/ambassador/ambassador.jpg')})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                    <Col lg="6">
                      <h1 className="display-3 text-white" style={{marginBottom: "100px"}}>
                        Стань Амбассадором{" "}
                        <span></span>
                      </h1>
                        <Button
                          className="mb-3 mb-sm-0"
                          href="https://forms.office.com/Pages/ResponsePage.aspx?id=zTRAbSVyck-4U5H-rqZJGegRNy8qPdhNvb6IK_zIuyRUMFZNTzBUQ1RBMDFBQjJTREZQT0VNTDY0Sy4u"
                          target="_blank"
                          color="here-yg"
                        >
                        <span className="btn-inner--text">Подать заявку</span>
                        </Button>
                     
                    </Col>
                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
              {/* 1st Hero Variation */}
            </div>
            <WhoAmbassador />
            <Reasons />
            {/* <TimeLine /> */}
            <Ideas/>
            <Tasks />

            <section className="section section-lg pt-10">
            <Container>
              <Card className="bg-gradient-here-yg shadow-lg border-0">
                <div className="p-5">
                  <Row className="align-items-center">
                    <Col lg="8">
                      <h3 className="text-white">
                        Стань амбассадором HERE Technologies!
                      </h3>
                      <p className="lead text-white mt-3">
                          Заполните форму участника, мы проанализируем анкету и свяжемся с Вами!
                      </p>
                    </Col>
                    <Col className="ml-lg-auto" lg="3">
                      <Button
                        block
                        className="btn-white"
                        color="default"
                        target="_blank"
                        href="https://forms.office.com/Pages/ResponsePage.aspx?id=zTRAbSVyck-4U5H-rqZJGegRNy8qPdhNvb6IK_zIuyRUMFZNTzBUQ1RBMDFBQjJTREZQT0VNTDY0Sy4u"
                        size="lg"
                      >
                        Подать заявку
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Container>
          </section>
          </main>
        <CardsFooter />
      </>
    )
  }
}

export default AmbassadorLanding
