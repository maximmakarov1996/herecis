import React from "react"

import {
  Button,
  Container,
  Row,
  Col,
} from "reactstrap"


import CardsFooter from "../components/CardsFooter"

// index page sections
import NavigationBar from '../components/NavigationBar'
import Opportunities from '../components/about/Opportunities'
import Community from '../components/about/Community'
import Blog from "../components/about/Blog"
import University from '../components/about/University'
import Channels from '../components/about/Channels'
import OurTeam from '../components/about/OurTeam'

class Landing extends React.Component {
  state = {};
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavigationBar />
        <main ref="main">
          <div className="position-relative">
            {/* shape Hero */}
            <section className="section section-lg section-shaped pb-250">
              <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require('assets/img/brand/community.jpg')})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                    <Col lg="6">
                      <h1 className="display-3 text-white">
                        HERE Community{" "}
                        <span></span>
                      </h1>
                      <p className="lead text-white" style={{fontWeight: '500'}}>
                        Сообщество единомышленников, развивающихся в области геоинформационных технологий
                      </p>
                      <div className="btn-wrapper">
                        <Button
                          className="btn-icon mb-3 mb-sm-0"
                          color="here-pa"
                          href="https://developer.here.com/events/community-russia"
                          target="_blank"
                        >
                          <span className="btn-inner--icon mr-1">
                            <i className="fa fa-code" />
                          </span>
                          <span className="btn-inner--text">Разработчикам</span>
                        </Button>
                        <Button
                          className="btn-white btn-icon mb-3 mb-sm-0 ml-1"
                          color="here-pa"
                          href="https://mapcreator.here.com/"
                          target="_blank"
                        >
                          <span className="btn-inner--icon mr-1">
                            <i className="ni ni-world-2" />
                          </span>
                          <span className="btn-inner--text">
                            Картографам
                          </span>
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
              {/* 1st Hero Variation */}
            </div>
        
            <Opportunities />
            <Community />
            <University />
            {/* <Blog /> */}
            <Channels />
            <OurTeam />
          </main>
        <CardsFooter />
      </>
    )
  }
}

export default Landing