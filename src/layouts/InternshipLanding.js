import React from "react"
import { Link } from "react-router-dom"

import {
  Button,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardBody,
} from "reactstrap"

import CardsFooter from "../components/CardsFooter"
import NavigationBar from '../components/NavigationBar'
import Schema from '../components/internship/Schema'
import SliderReasons from '../components/internship/SliderReasons'
import Prices from '../components/internship/Prices'



import final_image from 'assets/img/internship/final-image-club.svg'

import icon_first from '../assets/img/internship/icon-1.svg'
import icon_second from '../assets/img/internship/icon-2.svg'
import icon_third from '../assets/img/internship/icon-3.svg'



class InternshipLanding extends React.Component {
  state = {}
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
  }
  render() {
    return (
      <>
        <NavigationBar />
        <main ref="main">
          <div className="position-relative">
            {/* shape Hero */}
            <section className="section section-lg section-shaped pb-100">
              <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require("assets/img/internship/notebook-1971495_1920.jpg")})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                    <Col lg="8" style={{ background: '#383c45ed', padding: '40px'}}>
                      <h1 className="display-3 text-white" >
                        HERE Community Challenge #5{" "}
                      </h1>
                      <p className="lead text-white">
                        Картографический конкурс для активных людей,
                        которые любят карты и развиваться в сообществе единомышленников<br/> <br/>
                        Выиграйте подарки, получите опыт и попадите на оплачиваемую стажировку<br/><br/>
                        Конкурс продлится до 10 июня 2022 года 

                      </p>
                      <div className="btn-wrapper">
                        <Button
                          className="mb-3 mb-sm-0"
                          color="here-oa"
                          href="https://forms.office.com/r/HTJ9Yb0Q7d"
                          target="_blank"
                 
                        >
                          {/* <span className="btn-inner--icon mr-1">
                            <i className="fa fa-code" />
                          </span> */}
                          <span className="btn-inner--text">Принять участие</span>
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
              {/* 1st Hero Variation */}
            </div>
            <section className="section section-lg">
                <Container fluid={true}>
                    <Row className="text-center justify-content-center" style={{paddingBottom: "60px"}}>
                        <Col lg="10">
                            <h2 className="display-2 ">Возможности</h2>
                        
                        </Col>
                    </Row>
                    <Row className="text-center justify-content-center">
                        <Col lg="10">
                         <SliderReasons />
                        </Col>
                    </Row>
                </Container>
            </section>
           <Schema />
           <Prices />
          
           <section className="section section-lg section-shaped pb-250">
              <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require('assets/img/internship/final-competition-invet.png')})`}}>
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
                <span />
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row>
                  

                  <img src={final_image} style={{height: '12vh'}}/>

                  </Row>
                </div>
              </Container>
              {/* SVG separator */}
              <div className="separator separator-bottom separator-skew">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                  >
                    <polygon
                      className="fill-white"
                      points="2560 0 2560 100 0 100"
                    />
                  </svg>
                </div>
              </section>
            <section className="section section-lg pt-lg-0 mt--100">
                <Container>
                    <Row className="row-grid">
                        <Col lg="4">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                    <div style={{display: 'flex', justifyContent: 'center', marginBottom: '24px'}}>
                                      <img src={icon_first} style={{height: '100px', width: 'auto'}}/>
                                    </div>

                                    <h6 className="text-uppercase">
                                        Официальный договор
                                    </h6>
                                    {/* <p className="description mt-3">
                                        Развитие навыков для успешного трудоустройства
                                    </p> */}
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="4">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                    <div style={{display: 'flex', justifyContent: 'center', marginBottom: '24px'}}>
                                      <img src={icon_second} style={{height: '100px', width: 'auto'}}/>
                                    </div>

                                    <h6 className="text-uppercase">
                                        Дистанционный формат
                                    </h6>
                                    {/* <p className="description mt-3">
                                        Онлайн встречи с ТОП менеджерами компаний геоинформационных технологий
                                    </p> */}
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="4">
                            <Card className="card-lift--hover shadow border-0">
                                <CardBody className="py-5">
                                    <div style={{display: 'flex', justifyContent: 'center', marginBottom: '24px'}}>
                                      <img src={icon_third} style={{height: '100px', width: 'auto'}}/>
                                    </div>

                                    <h6 className="text-uppercase">
                                        Реальный проект компании
                                    </h6>
                                    {/* <p className="description mt-3">
                                        Обзоры рынка труда, его тенденций, рисков и перспектив
                                    </p> */}
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </section>

           {/* <InternshipTimeLine /> */}
           <section className="section section-lg pt-100">
            <Container>
              <Card className="bg-gradient-here-oa shadow-lg border-0">
                <div className="p-5">
                  <Row className="align-items-center">
                    <Col lg="8">
                      <h3 className="text-white">
                        Присоединяйся к конкурсу <br/> HERE Community Challenge #5!
                      </h3>
                      <p className="lead text-white mt-3">
                          Заполните форму, чтобы стать участником конкурса
                      </p>
                    </Col>
                    <Col className="ml-lg-auto" lg="3">
                      <Button
                        block
                        className="btn-white"
                        color="default"
                        target="_blank"
                        href="https://forms.office.com/r/HTJ9Yb0Q7d"
                        size="lg"
                      >
                        Принять участие
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Container>
          </section>

          </main>
        <CardsFooter />
      </>
    )
  }
}

export default InternshipLanding
