// Node Modules
import React, { useState } from "react"


import {
  Badge,
  Card,
  CardBody,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap"


import CardsFooter from "../components/CardsFooter"

// index page sections
import NavigationBar from '../components/NavigationBar'

export const OnlineUniversityLanding = props => {
  
  const [tutorials, loadTutorials] = useState([
    {
      title: 'Map Creator. Начало работы',
      description: 'Быстрый старт по работе в картографическом редакторе. Частые ошибки и поелзные советы',
      url: 'https://www.twilty.com/app/viewer/ccac1135-8d51-4962-94e2-f9e1b03c9e15'
    },
    {
      title: 'Map Alerts. Оповещения на карте',
      description: 'Проверка информации о дорогах, местах и номерах домов',
      url: 'https://twilty.com/app/viewer/a6bdce41-d7e7-47db-97b8-c82ae36f287c'
    },
    {
      title: 'HERE Studio. Основы Визуализации геоданных',
      description: 'Создание интерактивной карты без программирования',
      url: 'https://twilty.com/app/viewer/92f8a647-12e1-41c9-b81b-140f3b62484a'
    },
    {
      title: 'Технологии веб-скрейпинга на Python',
      description: 'Сбор, обработка и анализ геоданных из сети интернет',
      url: 'https://twilty.com/app/viewer/538dc55e-9ac1-4154-84e1-cdb9d9076b93'
    },
    {
      title: 'Интерактивная карта на Python',
      description: 'Создание визуализации геоданных с помощью библиотеки Folium',
      url: 'https://www.twilty.com/app/viewer/7e9e74ae-aef4-4dba-846b-cd8ce2a3a2f4'
    },
    {
      title: 'Геокодирование больших объемов данных ',
      description: 'Прямое и обратное, пакетное и асинхронное геокодирование',
      url: 'https://twilty.com/app/viewer/b8044459-a43a-47e6-a11a-fab1145fdc9b'
    },
  ])
  
  return (
    <>
      <NavigationBar />
      <main>
        <div className="position-relative">
          {/* shape Hero */}
          <section className="section section-lg section-shaped pb-150">
            <div className="shape shape-style-1 shape-here" style={{backgroundImage: `url(${require('assets/img/university/head_background_2.jpg')})`}}>
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
              <span />
            </div>
            <Container className="py-lg-md d-flex">
              <div className="col px-0">
                <Row>
                  <Col lg="6">
                    <h1 className="display-3 text-white">
                      HERE University{" "}
                      <span></span>
                    </h1>
                    <p className="lead text-white" style={{fontWeight: '500'}}>
                      HERE University - это набор поэтапных уроков, описывающих создание геолокационных веб и мобильных приложений с помощью HERE API/SDK
                    </p>
                  </Col>
                </Row>
              </div>
            </Container>
            {/* SVG separator */}
            <div className="separator separator-bottom separator-skew">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  preserveAspectRatio="none"
                  version="1.1"
                  viewBox="0 0 2560 100"
                  x="0"
                  y="0"
                >
                  <polygon
                    className="fill-white"
                    points="2560 0 2560 100 0 100"
                  />
                </svg>
              </div>
            </section>
            {/* 1st Hero Variation */}
          </div>
        </main>
        <section className="section section-lg pt-lg-0 mt--150">
              <Container>                  
                <Row className="row-grid">
                  {tutorials.map((item, index) => (
                    <Col key={index} lg="4" style={{marginTop: '20px'}}>
                      <Card className="shadow border-0">
                        <CardBody className="py-5">
                            <h6 className="text-uppercase">{item.title}</h6>
                            <p className="description mt-3">{item.description}</p>
                            <a target="_blank" href={item.url}>
                                <Button className="mt-4" color="here-da">Начать</Button>
                            </a>
                        </CardBody>
                        </Card>
                    </Col>
                  ))} 
                </Row>
              </Container>
          </section>
      <CardsFooter />
    </>
  )
}

export default OnlineUniversityLanding
