import React from "react"
import ReactDOM from "react-dom"
import { HashRouter, Route, Switch, Redirect, HashHistory, BrowserRouter } from "react-router-dom"

import "assets/vendor/nucleo/css/nucleo.css"
import "assets/vendor/font-awesome/css/font-awesome.min.css"
import "assets/css/argon-design-system-react.css"
import 'react-vertical-timeline-component/style.min.css'
import "assets/css/style.css"

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import Landing from "./layouts/Landing"
import AmbassadorLanding from "./layouts/AmbassadorLanding"
import InternshipLanding from "./layouts/InternshipLanding"
import OnlineUniversityLanding from "./layouts/OnlineUniversityLanding"
import DevChallenge from "./layouts/DevChallenge"
import TeachersProgramm from "./layouts/TeachersProgramm"
import ProjectsLayout from "./layouts/ProjectsLayout"

ReactDOM.render(
  <>
  <HashRouter>
    <Switch>
      <Route path="/" exact render={props => <Landing {...props} />} />
      <Route path="/ambassador" exact render={ props => <AmbassadorLanding {...props} />} />
      <Route path="/internship" exact render={ props => <InternshipLanding {...props} />} />
      <Route path="/university" exact render={ props => <OnlineUniversityLanding {...props} />} />
      <Route path="/challenge" exact render={ props => <DevChallenge {...props} />} />
      <Route path="/learning" exact render={ props => <TeachersProgramm {...props} />} />
      <Route path="/here-studio" exact render={ props => <ProjectsLayout {...props} />} />
    </Switch>
  </HashRouter>
  
  </>,
  document.getElementById("root")
)
