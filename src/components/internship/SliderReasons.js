import React, { Component } from "react"
import Slider from "react-slick"

import {
    Card,
    CardImg,
} from 'reactstrap'

export default class SliderReasons extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      focusOnSelect: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      className: "center",
      // centerPadding: "60px",
      centerMode: true,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 1680,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            className: "center",
            // centerPadding: "60px",
            centerMode: true,
            dots: true
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            className: "center",
            // centerPadding: "60px",
            centerMode: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            className: "center",
            // centerPadding: "60px",
            centerMode: true,
            dots: true
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            className: "center",
            // centerPadding: "60px",
            centerMode: true,
            dots: true
          }
        }
      ]
    }
    return (
      <div>
        <Slider {...settings}>
         
                <Card className="bg-white shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/internship/1Работа в команде.jpg")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h5 className="display-5 font-weight-bold">
                          Работать в команде профессионалов
                        </h5>
                       
                    
                    </blockquote>
                    
                </Card>
         
         
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/internship/2 Получить навыки.jpg")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h5 className="display-5 font-weight-bold">
                           Открыть для себя новые перспективы
                        </h5>
                    
                    </blockquote>
                </Card>
      
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/internship/3Сделай крутой проект.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h5 className="display-5 font-weight-bold">
                            Сделать крутой и полезный проект
                        </h5>
                    
                    </blockquote>
                </Card>
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/internship/4Получи рекомендацию.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h5 className="display-5 font-weight-bold">
                           Получить рекомендацию от компании
                        </h5>
                    
                    </blockquote>
                </Card>
        </Slider>
      </div>
    )
  }
}