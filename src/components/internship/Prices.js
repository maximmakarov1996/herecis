import React, { Component } from 'react'
import PricesSlider from './PricesSlider'

import {
    Container,
    Row,
    Col,
    Badge
} from 'reactstrap'

class Prices extends Component {
    render () {
        return (
            <section className="section section-lg">
              <Container fluid={true}>
                <Row className="row-grid align-items-center">
                  
                  <Col md="6" style={{paddingLeft:"4rem",paddingRight:"4rem"}}>
                    <div className="pr-md-5">
                    
                      <h3>Подарки </h3>
                      <p>
                        Каждый участник проекта Community Challenge #5 может получить подарки. 
                        Для этого необходимо выполнить нужное количество изменений на карте.
                      </p>
                      <br/>
                      <h5>Список подарков:</h5>
                      <ul className="list-unstyled mt-2">
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="success"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Сертификат - 5 000 изменений
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="success"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Футболка - 10 000 изменений
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="success"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Толстовка - 15 000 изменений
                              </h6>
                            </div>
                          </div>
                        </li>
                      </ul>
                      <br/>
                      <h5>Дополнительные подарки:</h5>
                      <ul className="list-unstyled mt-2">
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="primary"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Портативная аккустика
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="primary"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Беспроводное зарядное
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="primary"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Термостакан
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="primary"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Бутылка для воды
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3"
                                color="primary"
                              >
                                <i className="fa fa-check" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Дождевик
                              </h6>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </Col>
                  <Col md="6">
                    <PricesSlider />
                  </Col>
                </Row>
              </Container>
            </section>
        )
    }
}

export default Prices