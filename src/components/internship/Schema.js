import React, { Component } from 'react'

import {
    Card,
    CardBody,
    Container,
    Row,
    Col,
    Button
} from 'reactstrap'


class University extends Component {
    render () {
        return (
            <section className="section pb-0 bg-gradient-here-oa">
                <Container>
                    <Row className="row-grid  align-items-center">
                        <Col className="ml-lg-auto" lg="6">
                            <div className="d-flex px-3">
                                <div>
                                    <div className="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle">
                                        <i className="fa fa-calendar text-here-da" />
                                    </div>
                                </div>
                                <div className="pl-4">
                                    <h4 className="display-4 text-white">Схема проведения конкурса</h4>
                                    <p className="text-white">
                                        Мы подготовили для вас пошаговую инструкцию, которая поможет в Community Challenge #5
                                    </p>
                                </div>
                            </div>

                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-steps">
                                                1
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title here-steps-title-da">
                                                С чего начать?
                                            </h5>
                                            <p>
                                            Давайте познакомимся 😊 <br/>
                                            Заполните Форму участника 
                                            </p>
                                        </div>
                                    </div>
                                    <div style={{textAlign:"right"}}>
                                        <Button
                                            className="mt-4"
                                            href="https://forms.office.com/r/HTJ9Yb0Q7d"
                                            color="here-da-outline"
                                            target="_blank"
                                        >
                                            Принять участие
                                        </Button>
                                    </div>
                                    
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-steps">
                                                2
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title here-steps-title-da">
                                                Что изучить?
                                            </h5>
                                            <p>
                                                Онлайн обучение по работе в редакторе Map Creator. 
                                                После обучения пройдите тест
                                            </p>
                                        </div>
                                       
                                    </div>
                                    <div style={{textAlign:"right"}}>
                                        <Button
                                            className="mt-4"
                                            color="here-da-outline"
                                            href="https://www.twilty.com/app/viewer/ccac1135-8d51-4962-94e2-f9e1b03c9e15"
                                            target="_blank"
                                        >
                                            Онлайн урок
                                        </Button>
                                        <Button
                                            className="mt-4"
                                            color="here-da-outline"
                                            href="https://forms.gle/Dd8VpitSK7G7ouZJA"
                                            target="_blank"
                                        >
                                            Тест
                                        </Button>
                                    </div>
                                </CardBody>
                            </Card>                             
                        </Col>
                        <Col className="ml-lg-auto" lg="6">
                            

                            <Card className="shadow shadow-lg--hover">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-steps">
                                                3
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title here-steps-title-da">
                                                Начало работы
                                            </h5>
                                            <p>
                                                Задача – исправлять ошибки на карте в соответствии с официальными открытыми источниками. 
                                                Для этого выберайте город с населением менее 50 тысяч в России и СНГ. 
                                                Помните, добавленные вами данные не должны нарушать авторские права
                                            </p>
                                        </div>
                                       
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-steps">
                                                4
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title here-steps-title-da">
                                                Следим за новостями
                                            </h5>
                                            <p>
                                            Добавьтесь в нашу группу ВК HERE Community и в телеграм чат HERE Community Challenge. 
                                            В группе будем публиковать итоги месяца. В чате будем публиковать недельный рейтинг

                                            </p>
                                        </div>
                                    </div>
                                    <div style={{textAlign:"right"}}>
                                        <Button
                                            className="mt-4"
                                            color="here-da-outline"
                                            href="https://t.me/here_community"
                                            target="_blank"
                                        >
                                            Телеграм чат
                                        </Button>
                                        <Button
                                            className="mt-4"
                                            color="here-da-outline"
                                            href="https://vk.com/heremc"
                                            target="_blank"
                                        >
                                            Группа вконтакте
                                        </Button>
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape here-da-steps">
                                                5
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title here-steps-title-da">
                                                А дальше..
                                            </h5>
                                            <p>
                                                Проект с 10 февраля по 10 июня 2022 года. Потом подведём итоги – награждение и отбор на оплачиваемую стажировку
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                           
                            
                        </Col>
                    </Row>
                </Container>
                {/* SVG separator */}
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="none"
                        version="1.1"
                        viewBox="0 0 2560 100"
                        x="0"
                        y="0"
                    >
                        <polygon
                        className="fill-white"
                        points="2560 0 2560 100 0 100"
                        />
                    </svg>
                </div>
            </section>
        )
    }
}

export default University