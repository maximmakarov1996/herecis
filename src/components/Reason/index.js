import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
    Badge
} from 'reactstrap'

export const Reason = props => {
    
    let { 
        icon, 
        title, 
        description,
        badge_title, 
        badge_list,
        img, 
    } = props

    return (
        <section className="section section-lg">
            <Container>
                <Row className="row-grid align-items-center">
                    <Col className="order-md-2" md="6">
                    <img
                        alt="..."
                        className="img-fluid shadow"
                        src={img}
                    />
                    </Col>
                    <Col className="order-md-1" md="6">
                    <div className="pr-md-5">
                        <div className="icon icon-lg icon-shape here-da shadow rounded-circle mb-5">
                        <i className={icon}/>
                        </div>
                            <h5>{title}</h5>
                        <p>
                            {description}
                        </p>
                        <h5>{badge_title}</h5>
                        <ul className="list-unstyled mt-2">
                            {badge_list.map( (item, index) => {
                                return (
                                    <li className="py-2" key={index}>
                                        <div className="d-flex align-items-center">
                                            <div>
                                                <Badge className="badge-circle mr-3 here-da">
                                                    <i className={item.icon} />
                                                </Badge>
                                            </div>
                                            <div>
                                                <h6 className="mb-0">{item.title}</h6>
                                            </div>
                                        </div>
                                </li>
                                )
                            })}
                        
                        </ul>
                    </div>
                    </Col>
                </Row>
            </Container>
        </section>
    )
    
}