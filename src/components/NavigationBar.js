import React from "react"
import { Link } from "react-router-dom"

import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

class NavigationBar extends React.Component {
  render() {
    return (
      <>
        <Navbar
          className="navbar-main navbar-light"
          expand="lg"
          id="navbar-main"
        >
          <Container>
            <NavbarBrand 
                expand="lg">
                <Link to="/">
                  <img
                    alt="..."
                    src={require("assets/img/brand/logo.png")}
                  />
                </Link>
            </NavbarBrand>
            <button
              aria-controls="navbar-default"
              aria-expanded={false}
              className="navbar-toggler"
              data-target="#navbar-main"
              data-toggle="collapse"
              id="navbar-default"
              type="button"
            >
              <i className="fa fa-bars" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-default">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <Link to="/">
                      <img
                        alt="..."
                        src={require("assets/img/brand/logo.png")}
                      />
                    </Link>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button
                      aria-controls="navbar-default"
                      aria-expanded={false}
                      aria-label="Toggle navigation"
                      className="navbar-toggler"
                      data-target="#navbar-default"
                      data-toggle="collapse"
                      id="navbar-default"
                      type="button"
                    >
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-lg-auto" navbar>
              <NavItem className="align-items-left">
                    <NavLink href="/#">
                        <span className="nav-link-inner--text">Главная</span>
                    </NavLink>
                </NavItem>
                <NavItem className="align-items-left">
                    <NavLink href="/#/university">
                        <span className="nav-link-inner--text">Онлайн университет</span>
                    </NavLink>
                </NavItem>
                <NavItem className="align-items-left">
                    <NavLink href="/#/here-studio">
                        <span className="nav-link-inner--text">Проекты</span>
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                      className="nav-link-icon"
                      href="https://vk.com/heremc"
                      id="tooltip333589074"
                      target="_blank"
                    >
                      <i className="fa fa-vk" />
                      <span className="nav-link-inner--text d-lg-none ml-2">
                        Вконтакте
                      </span>
                    </NavLink>
                    <UncontrolledTooltip delay={0} target="tooltip333589074">
                      Следите за новостями в группе Вконтакте
                    </UncontrolledTooltip>
                </NavItem>
                <NavItem>
                    <NavLink
                      className="nav-link-icon"
                      href="https://www.instagram.com/community_here/"
                      id="tooltip356693867"
                      target="_blank"
                    >
                      <i className="fa fa-instagram" />
                      <span className="nav-link-inner--text d-lg-none ml-2">
                        Instagram
                      </span>
                    </NavLink>
                    <UncontrolledTooltip delay={0} target="tooltip356693867">
                      Фото с мероприятий HERE Technologies
                    </UncontrolledTooltip>
                </NavItem>
                <NavItem>
                    <NavLink
                        className="nav-link-icon"
                        href="https://www.youtube.com/channel/UCfvY-onxkk250fp2KN2xMGg"
                        id="tooltip112445449"
                        target="_blank"
                    >
                        <i className="fa fa-youtube" />
                        <span className="nav-link-inner--text d-lg-none ml-2">
                        Youtube
                        </span>
                    </NavLink>
                    <UncontrolledTooltip delay={0} target="tooltip112445449">
                        Канал HERE Community с видеоуроками по платформе
                    </UncontrolledTooltip>
                </NavItem>
                {/* <NavItem> 
                  <NavLink
                      href="http://mapmyyear.xyz/"
                      target="_blank"
                      id="map-my-year"
                      style={{paddingTop: "0.8rem",  paddingBottom: "0rem"}}
                    
                  >
                      <img
                      alt="..."
                      style={{height:"30px"}}
                      src={require("assets/img/brand/logo_MMY_text.png")}
                      />
                  </NavLink>
                </NavItem>  */}
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default NavigationBar;