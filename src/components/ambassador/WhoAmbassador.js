import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
    Badge
} from 'reactstrap'

class WhoAmbassador extends Component {
    render () {
        return (
            <section className="section section-lg">
              <Container>
                <Row className="row-grid align-items-center">
                  <Col className="order-md-2" md="6">
                    <img
                      alt="..."
                      className="img-fluid shadow"
                      src={require("assets/img/ambassador/DAC_5489.JPG")}
                    />
                  </Col>
                  <Col className="order-md-1" md="6">
                    <div className="pr-md-5">
                      <div className="icon icon-lg icon-shape here-yg shadow rounded-circle mb-5">
                        <i className="ni ni-hat-3" />
                      </div>
                      <h3>Кто такой амбассадор компании HERE Technologies?</h3>
                      <p>
                        Амбассадорами становятся уже хорошо знакомые с компанией представители университетов, которые принимали участие в различных проектах.
                      </p>
                      <p className="mb-5">
                        Cтуденты, аспиранты, магистры, преподаватели университетов, увлеченные картографией, геолокацией или программированием.​
                      </p>
                      
                      <h5>Полезные навыки:</h5>
                      <ul className="list-unstyled mt-2">
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3 here-yg"
                              >
                                <i className="fa fa-users" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Организаторский опыт
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3 here-yg"
                                color="success"
                              >
                                <i className="fa fa-globe" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">Работа в международной команде</h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3 here-yg"
                                color="success"
                              >
                                <i className="fa fa-bullhorn" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                                Публичные выступления
                              </h6>
                            </div>
                          </div>
                        </li>
                        <li className="py-2">
                          <div className="d-flex align-items-center">
                            <div>
                              <Badge
                                className="badge-circle mr-3 here-yg"
                                color="success"
                              >
                                <i className="fa fa-map-o" />
                              </Badge>
                            </div>
                            <div>
                              <h6 className="mb-0">
                              Soft Skills
                              </h6>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </Col>
                </Row>
              </Container>
            </section>
        )
    }
}

export default WhoAmbassador