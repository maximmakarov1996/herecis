import React, { Component } from 'react'

import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component'


import {
    Container,
    Card,
} from 'reactstrap'

class TimeLine extends Component {
    render () {
        return (
            <section className="section section-lg  pb-0">
                <Container>
                    <Card style={{border: "0", borderBottom: "4px solid #85e0ce"}}>
                        <div className="p-5">
                            <h2 className="display-2  text-center">
                                Этапы отбора
                            </h2>
                        </div>
                    </Card>
                    <VerticalTimeline style={{border: "0", borderBottom: "4px solid #85e0ce"}}>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={{ background: '#85e0ce', color: '#fff' }}
                            contentArrowStyle={{ borderRight: '7px solid  #85e0ce' }}
                            iconStyle={{ background: '#fff', color: '#fff', border:"13px solid #85e0ce" }}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Шаг 1</h3>
                            <p>
                                Подай заявку на сайте.
                                Регистрация доступна до 30 апреля в 23:59 по московскому времени.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={{ background: '#85e0ce', color: '#fff' }}
                            contentArrowStyle={{ borderRight: '7px solid  #85e0ce' }}
                            iconStyle={{ background: '#fff', color: '#fff', border:"13px solid #85e0ce" }}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Шаг 2</h3>
                            <p>
                                Проверка заявки и проведение отборочного собеседования.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={{ background: '#85e0ce', color: '#fff' }}
                            contentArrowStyle={{ borderRight: '7px solid  #85e0ce' }}
                            iconStyle={{ background: '#fff', color: '#fff', border:"13px solid #85e0ce" }}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Шаг 3</h3>
                            <p>
                                Прохождение образовательного курса по основам геосервисов HERE и презентационным навыкам. Изучение информации рассчитано на июнь-июль месяц.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={{ background: '#85e0ce', color: '#fff' }}
                            contentArrowStyle={{ borderRight: '7px solid  #85e0ce' }}
                            iconStyle={{ background: '#fff', color: '#fff', border:"13px solid #85e0ce" }}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Шаг 4</h3>
                            <p>
                                Начало взаимодействия с компанией HERE, участие в митапах и организация проектов.
                            </p>
                        </VerticalTimelineElement>
                        <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            contentStyle={{ background: '#85e0ce', color: '#fff' }}
                            contentArrowStyle={{ borderRight: '7px solid  #85e0ce' }}
                            iconStyle={{ background: '#fff', color: '#fff', border:"13px solid #85e0ce" }}
                        >
                            <h3 className="vertical-timeline-element-title text-white">Шаг 5</h3>
                            <p>
                                Награждение наиболее активных участников по итогам года.
                            </p>
                        </VerticalTimelineElement>
                    </VerticalTimeline>
                </Container>    
            </section>
        )
    }
}

export default TimeLine