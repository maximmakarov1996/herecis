import React, { Component } from "react"
import Slider from "react-slick"

import {
    Col,
    Card,
    CardImg
} from 'reactstrap'

export default class SimpleSlider extends Component {
  render() {
    const settings = {
        dots: true,
        infinite: true,
        focusOnSelect: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        className: "center",
        // centerPadding: "60px",
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 1680,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              className: "center",
              // centerPadding: "60px",
              centerMode: true,
              dots: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              className: "center",
              // centerPadding: "60px",
              centerMode: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              className: "center",
              // centerPadding: "60px",
              centerMode: true,
              dots: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              className: "center",
              // centerPadding: "60px",
              centerMode: true,
              dots: true
            }
          }
        ]
      }
    return (
      <div>
        <Slider {...settings}>
         
         
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/ambassador/2Посети офис компании.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h4 className="display-4 font-weight-bold">
                            Посети офис компании
                        </h4>
                    
                    </blockquote>
                </Card>
      
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/ambassador/3Развивайся.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h4 className="display-4 font-weight-bold">
                            Развивайся
                        </h4>
                    
                    </blockquote>
                </Card>
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/ambassador/4Получимоднуюсувенирку.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h4 className="display-4 font-weight-bold">
                            Получи модную сувенирку
                        </h4>
                    
                    </blockquote>
                </Card>
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/ambassador/5Найди ценные контакты.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h4 className="display-4 font-weight-bold">
                            Найди ценные контакты
                        </h4>
                    
                    </blockquote>
                </Card>
                <Card className="shadow border-0"  >
                    <CardImg
                        alt="..."
                        src={require("assets/img/ambassador/6Стань частью семьи.JPG")}
                        top
                    />
                    <blockquote className="card-blockquote">
                        <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                        ></svg>
                        <h4 className="display-5 font-weight-bold">
                            Стань частью семьи HERE
                        </h4>
                    
                    </blockquote>
                </Card>
       
        </Slider>
      </div>
    )
  }
}