import React, { Component } from 'react'

import {
    Card,
    CardBody,
    Container,
    Row,
    Col,
} from 'reactstrap'

import icon_1 from '../../assets/img/ambassador/am-icon-1.svg'
import icon_2 from '../../assets/img/ambassador/am-icon-2.svg'
import icon_3 from '../../assets/img/ambassador/am-icon-3.svg'
import icon_4 from '../../assets/img/ambassador/am-icon-4.svg'

class Ideas extends Component {
    render () {
        return (
            <section className="section pb-20 bg-gradient-here-da">
                <Container>
                    <Row className="row-grid  align-items-center">
                    <Col lg="6">
                            <div className="d-flex px-3">
                                <div>
                                    <div className="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle">
                                        <i className="ni ni-paper-diploma text-here-da" />
                                    </div>
                                </div>
                                <div className="pl-4">
                                    <h4 className="display-4 text-white">HERE Ambassador</h4>
                                    <p className="text-white">
                                        В программе амбассадоров есть два направления: IT и картография. 
                                        Участники программ выполняют различные задания. На основе активности формируется рейтинг. Раз в пол года подводим итог и награждаем подарками.

                                    </p>
                                </div>
                            </div>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape">
                                                <img src={icon_1} style={{width: "80px"}}/>
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                                Онлайн встречи
                                            </h5>
                                            <p>
                                                Участие в звонках и активность. Для картографов-амбассадоров звонки проводятся 1 раз в месяц. Для IT – 1 раз в 2 недели.
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                <div className="d-flex px-3">
                                    <div>
                                        <div className="icon icon-shape ">
                                            <img src={icon_2} style={{width: "80px"}}/>

                                        </div>
                                    </div>
                                    <div className="pl-4">
                                        <h5 className="title text-here-da">
                                        Инициативы
                                        </h5>
                                        <p>
                                        Привлечение новых участников в проекты, организация сотрудничества с ВУЗами, реализованные идеи по проектам и конкурсам
                                        </p>
                                    </div>
                                </div>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col lg="6">
                            <Card className="shadow shadow-lg--hover">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape">
                                                <img src={icon_3} style={{width: "80px"}}/>
                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                                Выступления
                                            </h5>
                                            <p>
                                            Каждый амбассадор выступает для коллег и сообщества. Так тренируются навыки публичного выступления
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                            <Card className="shadow shadow-lg--hover mt-5">
                                <CardBody>
                                    <div className="d-flex px-3">
                                        <div>
                                            <div className="icon icon-shape">
                                                <img src={icon_4} style={{width: "80px"}}/>

                                            </div>
                                        </div>
                                        <div className="pl-4">
                                            <h5 className="title text-here-da">
                                            Поддержка
                                            </h5>
                                            <p>
                                            Помощь новым участникам проектов – отвечаем на вопросы, помогаем решить трудности с заданием. Поддерживаем друг друга в начинаниях.
                                            </p>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
                {/* SVG separator */}
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="none"
                        version="1.1"
                        viewBox="0 0 2560 100"
                        x="0"
                        y="0"
                    >
                        <polygon
                        className="fill-white"
                        points="2560 0 2560 100 0 100"
                        />
                    </svg>
                </div>
            </section>
        )
    }
}

export default Ideas