import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
    CardBody,
    Card,
    CardImg
  } from "reactstrap"



class Tasks extends Component {


    render () {
        return (  
            <section className="section pb-100 ">
                <Container>
                  <Row className="text-center justify-content-center">
                    <Col lg="10">
                      <h2 className="display-3 ">Задачи и обязанности</h2>
                    </Col>
                  </Row>
                  <Row className="row-grid  align-items-center">
                      <Col className="ml-lg-auto" md="6">
                          <Card className="shadow shadow-lg--hover mt-5">
                            <CardImg
                                alt="..."
                                src={require("assets/img/ambassador/tech_learn.jpg")}
                                top
                              />
                              <CardBody>
                                  <div className="d-flex px-3">
                                      <div className="pl-4">
                                          <h5 className="title text-here-da">
                                              Изучение технологий
                                          </h5>
                                          <p>
                                            Изучить сервисы HERE, необходимые для участия в программе:​ HERE Map Creator, HERE Studio.​
                                          </p>
                                      </div>
                                  </div>
                              </CardBody>
                          </Card>
                          <Card className="shadow shadow-lg--hover mt-5">
                              <CardImg
                                alt="..."
                                src={require("assets/img/ambassador/learn.jpg")}
                                top
                              />
                              <CardBody>
                                  <div className="d-flex px-3">
                                      <div className="pl-4">
                                          <h5 className="title text-here-da">
                                            Управление проектами
                                          </h5>
                                          <p>
                                            Курировать работу проектной группы:​ проведение встреч;​ 
                                            помощь в освоении технологий;​
                                            коммуникация с сотрудниками компании;​
                                            награждение победителей по итогам месяца.​
                                          </p>
                                      </div>
                                  </div>
                              </CardBody>
                          </Card>
                      </Col>
                      <Col lg="6">
                          <Card className="shadow shadow-lg--hover">
                              <CardImg
                                alt="..."
                                src={require("assets/img/ambassador/meeting_organization_2.jpg")}
                                top
                              />
                              <CardBody>
                                  <div className="d-flex px-3">
                                      <div className="pl-4">
                                          <h5 className="title text-here-da">
                                              Организация встреч
                                          </h5>
                                          <p>
                                            Проводить встречи 2 раза в месяц со студентами для разбора задания и награждения участников.
                                          </p>
                                      </div>
                                  </div>
                              </CardBody>
                          </Card>
                          <Card className="shadow shadow-lg--hover mt-5">
                              <CardImg
                                alt="..."
                                src={require("assets/img/ambassador/2.JPG")}
                                top
                              />
                              <CardBody>
                              <div className="d-flex px-3">
                                  <div className="pl-4">
                                      <h5 className="title text-here-da">
                                      Выступления для сообщества
                                      </h5>
                                      <p>
                                      Участвовать в звонках, выступать для коллег амбассадоров и всего сообщества. Готовить материалы, презентации для вебинаров. Записывать видеоролики и участвовать в подготовке контента ддля группы.
                                      </p>
                                  </div>
                              </div>
                              </CardBody>
                          </Card>
                      </Col>
                  </Row>
                </Container>
                {/* SVG separator */}
                <div className="separator separator-bottom separator-skew zindex-100">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="none"
                        version="1.1"
                        viewBox="0 0 2560 100"
                        x="0"
                        y="0"
                    >
                        <polygon
                        className="fill-white"
                        points="2560 0 2560 100 0 100"
                        />
                    </svg>
                </div>
          </section>
          
        )
    }
}

export default Tasks