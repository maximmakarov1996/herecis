import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
} from 'reactstrap'

import Slider from './OpportunitiesSlider'

class Reasons extends Component {
    render () {
        return (
            <section className="section section-lg bg-gradient-here-yg">
                <Container fluid={true}>
                    <Row className="text-center justify-content-center" style={{paddingBottom: "60px"}}>
                        <Col lg="10">
                            <h2 className="display-2 ">Возможности</h2>
                        
                        </Col>
                    </Row>
                    <Row className="text-center justify-content-center">
                        <Col lg="10">
                            <Slider />
                        </Col>
                    </Row>
                </Container>
               
            </section>
        )
    }
}

export default Reasons