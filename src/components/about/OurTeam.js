import React, { Component } from 'react'

import {
    Button,
    Container,
    Row,
    Col,
  } from "reactstrap"

class OurTeam extends Component {
    render () {
        return (
            <section className="section section-lg bg-secondary">
                <Container>
                    <Row className="justify-content-center text-center mb-lg">
                        <Col lg="8">
                            <h2 className="display-3">Представители</h2>
                            <p className="lead text-muted">
                                В России и Казахстане офисы HERE Technologies находятся в Москве, Ростове-на-Дону, Санкт-Петербурге, Нурсултане.
                                Вы всегда можете связаться с сотрудниками компании в социальных сетях.
                            </p>
                        </Col>
                    </Row>
                    <Row className="justify-content-center text-center mb-lg">
                        <Col className="mb-5 mb-lg-0" lg="3" md="6">
                            <div className="px-2">
                                <img
                                    alt="..."
                                    className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                    src={require("assets/img/team/max_makarov.jpg")}
                                    style={{ width: "200px" }}
                                />
                                <div className="pt-4 text-center">
                                <h5 className="title">
                                    <span className="d-block mb-1">Максим Макаров</span>
                                    <small className="h6 text-muted">Community Analyst</small>
                                </h5>
                                <div className="mt-3">
                                    <Button
                                        className="btn-icon-only rounded-circle"
                                        href="https://vk.com/maxcrosh"
                                        target="_blank"
                                        color="vk-logo-color"
                                    >
                                        <i className="fa fa-vk" />
                                    </Button>
                                    <Button
                                        className="btn-icon-only rounded-circle ml-1"
                                        href="https://www.facebook.com/max.makarov.127"
                                        target="_blank"
                                        color="fb-logo-color"
                                    >
                                        <i className="fa fa-facebook" />
                                    </Button>
                                </div>
                                </div>
                            </div>
                        </Col>
                        <Col className="mb-5 mb-lg-0" lg="3" md="6">
                            <div className="px-2">
                                <img
                                    alt="..."
                                    className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                    src={require("assets/img/team/viktoria_ermakova.jpg")}
                                    style={{ width: "200px" }}
                                />
                                <div className="pt-4 text-center">
                                <h5 className="title">
                                    <span className="d-block mb-1">Виктория Ермакова</span>
                                    <small className="h6 text-muted">Community Analyst</small>
                                </h5>
                                <div className="mt-3">
                                    <Button
                                        className="btn-icon-only rounded-circle"
                                        href="https://vk.com/vikaa_ermakova"
                                        target="_blank"
                                        color="vk-logo-color"
                                    >
                                        <i className="fa fa-vk" />
                                    </Button>
                                    <Button
                                        className="btn-icon-only rounded-circle ml-1"
                                        href="https://www.facebook.com/profile.php?id=100015905734716"
                                        target="_blank"
                                        color="fb-logo-color"
                                    >
                                        <i className="fa fa-facebook" />
                                    </Button>
                                </div>
                                </div>
                            </div>
                        </Col>
                        <Col className="mb-5 mb-lg-0" lg="3" md="6">
                            <div className="px-2">
                                <img
                                    alt="..."
                                    className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                    src={require("assets/img/team/olesia_nikolaeva.jpg")}
                                    style={{ width: "200px" }}
                                />
                                <div className="pt-4 text-center">
                                <h5 className="title">
                                    <span className="d-block mb-1">Олеся Николаева</span>
                                    <small className="h6 text-muted">Community Analyst</small>
                                </h5>
                                <div className="mt-3">
                                    <Button
                                        className="btn-icon-only rounded-circle"
                                        href="https://vk.com/osani"
                                        target="_blank"
                                        color="vk-logo-color"
                                    >
                                        <i className="fa fa-vk" />
                                    </Button>
                                    <Button
                                        className="btn-icon-only rounded-circle ml-1"
                                        href="https://www.facebook.com/olesya.nikolaeva"
                                        target="_blank"
                                        color="fb-logo-color"
                                    >
                                        <i className="fa fa-facebook" />
                                    </Button>
                                </div>
                                </div>
                            </div>
                        </Col>
                        <Col className="mb-5 mb-lg-0" lg="3" md="6">
                            <div className="px-2">
                                <img
                                    alt="..."
                                    className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                    src={require("assets/img/team/azhar_nurtazina.jpg")}
                                    style={{ width: "200px" }}
                                />
                                <div className="pt-4 text-center">
                                <h5 className="title">
                                    <span className="d-block mb-1">Ажар Нуртазина</span>
                                    <small className="h6 text-muted">Central Asia Courator</small>
                                </h5>
                                <div className="mt-3">
                                    <Button
                                        className="btn-icon-only rounded-circle ml-1"
                                        href="https://www.facebook.com/profile.php?id=100013484647470"
                                        target="_blank"
                                        color="fb-logo-color"
                                    >
                                        <i className="fa fa-facebook" />
                                    </Button>
                                </div>
                                </div>
                            </div>
                        </Col>
                        {/* <Col className="mb-5 mb-lg-0" lg="3" md="6">
                            <div className="px-2">
                                <img
                                    alt="..."
                                    className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                    src={require("assets/img/team/maksim_makarov.jpg")}
                                    style={{ width: "200px" }}
                                />
                                <div className="pt-4 text-center">
                                <h5 className="title">
                                    <span className="d-block mb-1">Максим Макаров</span>
                                    <small className="h6 text-muted">Community Analyst</small>
                                </h5>
                                <div className="mt-3">
                                    <Button
                                        className="btn-icon-only rounded-circle vk-logo-color"
                                        href="https://vk.com/maxkpow"
                                        target="_blank"
                                    >
                                        <i className="fa fa-vk" />
                                    </Button>
                                    <Button
                                        className="btn-icon-only rounded-circle ml-1 fb-logo-color"
                                        href="https://www.facebook.com/max.makarov.127"
                                        target="_blank"
                                    >
                                        <i className="fa fa-facebook" />
                                    </Button>
                                </div>
                                </div>
                            </div>
                        </Col> */}
                    </Row>
                </Container>
            </section>
        )
    }
}

export default OurTeam