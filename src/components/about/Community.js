import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
    UncontrolledCarousel
  } from "reactstrap"

const items = [
    {
        src: require("assets/img/community/international_practice.jpg"),
        altText: '',
        caption: 'Компания HERE Technologies регулярно организовывает международные проекты - отличная возможность для обмена опытом с заружебными коллегами',
        header: 'HERE Interantional Camp Gdansk  2017'
    },
    {
        src: require("assets/img/community/here_sochi_camp.jpg"),
        altText: '',
        caption: 'Международный лагерь для студентов IT и ГИС специальностей из России, Казахстана и Беларуси',
        header: 'HERE Sochi Camp 2019'
    },
    {
        src: require("assets/img/community/hack_moscow.jpg"),
        altText: '',
        caption: 'Участие в роли партнера с номинацией HERE We Are',
        header: 'Hack Moscow v3.0'
    },
    {
        src: require("assets/img/community/mapathon.jpg"),
        altText: '',
        caption: 'HERE Mapathon в Ставрополе',
        header: 'HERE Mapathon 2019 Stavropol'
    }
]

class Community extends Component {
    render () {
        return (
            <section className="section ">
                <Container>
                    <Row className="text-center justify-content-center">
                        <Col lg="10">
                            <h2 className="display-3 ">Проекты сообщества</h2>
                            <p className="lead ">
                                Участники создают цифровую навигационную основу, занимаются разработкой веб и мобильных приложений с использованием геолокационных технологий, принимают участие в глобальных проектах и выигрывают призы 
                            </p>
                        </Col>
                    </Row>
                    <Row className="text-center justify-content-center">
                        <Col lg="12">
                            <UncontrolledCarousel items={items} style={{color: "#fff"}}/>
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Community