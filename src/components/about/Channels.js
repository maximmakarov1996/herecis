import React, { Component } from 'react'

import {
    Container,
    Row,
    Col,
  } from "reactstrap"

class OurTeam extends Component {
    render () {
        return (
            <section className="section section-lg">
                <Container>
                    <Row className="justify-content-center text-center mb-lg">
                        <Col lg="8">
                            <h2 className="display-3">Сообщество</h2>
                            <p className="lead text-muted">
                                У вас возник вопрос? Мы всегда на связи!
                            </p>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="mb-5 mb-lg-0" lg="4" md="6">
                            <a 
                                href="https://vk.com/heremc" 
                                target="_blank"  rel="noopener noreferrer"
                            >
                                <div className="px-4">
                                    <img
                                        alt="..."
                                        className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                        src={require("assets/img/community/vk.png")}
                                        style={{ width: "100px" }}
                                    />
                                    <div className="pt-4 text-center">
                                        <h5 className="title">
                                            <span className="d-block mb-1">Вконтакте</span>
                                            <small className="h6 text-muted">
                                                Актуальные новости</small>
                                        </h5>
                                    </div>
                                </div>
                            </a>
                        </Col>
                        <Col className="mb-5 mb-lg-0" lg="4" md="6">
                            <a 
                                href="https://t.me/heredev" 
                                target="_blank"  rel="noopener noreferrer"
                            >
                                <div className="px-4">
                                    <img
                                        alt="..."
                                        className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                        src={require("assets/img/community/telegram.png")}
                                        style={{ width: "100px" }}
                                    />
                                    <div className="pt-4 text-center">
                                        <h5 className="title">
                                            <span className="d-block mb-1">Телеграм</span>
                                            <small className="h6 text-muted">
                                                Техническая поддержка
                                            </small>
                                        </h5>
                                    </div>
                                </div>
                            </a>
                        </Col>
                        <Col className="mb-5 mb-lg-0" lg="4" md="6">
                            <a 
                                href="https://t.me/here_community" 
                                target="_blank"  rel="noopener noreferrer"
                            >
                                <div className="px-4">
                                    <img
                                        alt="..."
                                        className="rounded-circle img-center img-fluid shadow shadow-lg--hover"
                                        src={require("assets/img/community/telegram.png")}
                                        style={{ width: "100px" }}
                                    />
                                    <div className="pt-4 text-center">
                                        <h5 className="title">
                                            <span className="d-block mb-1">HERE Community Challenge</span>
                                            <small className="h6 text-muted">Чат картографов</small>
                                        </h5>
                                    </div>
                                </div>
                            </a>
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

export default OurTeam